#include "pocketbook_types.hpp"

#include <regex>

namespace getnotes2::hl_types {
void to_json(json &j, const location &l) {
  j = json{{"anchor", l.anchor}, {"created", l.created_timestamp}};
}

void from_json(const json &j, location &l) {
  j.at("anchor").get_to(l.anchor);
  j.at("created").get_to(l.created_timestamp);
}

void to_json(json &j, const note_text &l) { j = json{{"text", l.text}}; }

void from_json(const json &j, note_text &l) { j.at("text").get_to(l.text); }

void to_json(json &j, const highlight_text &l) {
  j = json{{"begin", l.begin}, {"end", l.end}, {"text", l.text}};
}

void from_json(const json &j, highlight_text &l) {
  j.at("begin").get_to(l.begin);
  j.at("end").get_to(l.end);
  j.at("text").get_to(l.text);
}

  /*NLOHMANN_JSON_SERIALIZE_ENUM(types, {
                                        {note, "note"},
                                        {highlight, "highlight"},
                                        {bookmark, "bookmark"},
                                        {bookmark, "yellow"},
					})*/


std::optional<parsed_tag> parse_tag(const db::tag& input){
  parsed_tag ret;
  ret.tag_id = input.tag_id;
  auto j = json::parse(input.val);
  try {
    ret.val = j.get<highlight_text>();
    return ret;
  } catch(const nlohmann::detail::out_of_range& ex) {
    try {
      ret.val = j.get<location>();
      return ret;
    } catch(const nlohmann::detail::out_of_range& ex) {
      ret.val = j.get<note_text>();
      return ret;
      try {
      } catch(const nlohmann::detail::out_of_range& ex) {
	return std::optional<parsed_tag>();
      }
    }
  }
}

  static std::regex page_regex("\\?page=([0-9]*)\\&");
  
  int parse_page_in_anchor(const std::string &anchor){
    std::smatch m;
    std::regex_search(anchor,m, page_regex);
    return std::stoi(m[1])+1;
  }

  
} // namespace getnotes2::hl_types
