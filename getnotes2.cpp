#include <algorithm>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <set>
#include <utility>

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/program_options.hpp>

#include "formatters.hpp"
#include "pocketbook_tables.hpp"
#include "pocketbook_types.hpp"

#include <sqlite3/soci-sqlite3.h>

using namespace soci;
using namespace getnotes2;

namespace po = boost::program_options;

void find_books(session &sql, const std::string &title_part,
                const getnotes2::output::formatter &fmt) {
  auto res = db::get_books_by_title_part(sql, title_part);

  fmt.format_book_list(res);
}

void find_notes(session &sql, int book_id,
                const getnotes2::output::formatter &fmt) {
  auto res = db::get_tags_by_book_id(sql, book_id);
  auto tags = hl_types::parse_tag_list(res.begin(), res.end());
  fmt.format_notes(tags);
}

void clear_orphaned_items(session &sql, bool dry_run) {
  std::set<int> ids;
  std::multimap<int, int> parent_to_children;

  for (const db::item &i : db::get_all_items(sql)) {
    ids.emplace(i.oid);
    if (i.parent)
      parent_to_children.insert(std::make_pair(*(i.parent), i.oid));
  }

  // C++20 Erase if, currently using c++17
  for (auto i = parent_to_children.begin(), last = parent_to_children.end();
       i != last;) {
    if (ids.find((*i).first) != ids.end()) {
      i = parent_to_children.erase(i);
    } else {
      ++i;
    }
  }

  if (dry_run) {
    std::cout << "Items to delete ";
    std::ostream_iterator<int> it(std::cout, ", ");
    std::transform(parent_to_children.begin(), parent_to_children.end(), it,
                   [](auto &p) { return p.second; });
    std::cout << std::endl;
  } else {
    std::vector<int> ids_to_del;
    auto it = std::back_inserter(ids_to_del);
    std::transform(parent_to_children.begin(), parent_to_children.end(), it,
                   [](auto &p) { return p.second; });
    db::delete_items_by_oid(sql, ids_to_del);
  }
}

void clear_books(session &sql, const std::string &directory_path,
                 bool dry_run) {
  bool found_existing = false;
  std::set<int> files_to_clear;
  std::set<int> items_to_clear_probably;
  std::set<int> items_not_to_clear_eventually;
  db::for_each_file_in_db(
      sql, [&directory_path, &files_to_clear, &items_to_clear_probably,
            &items_not_to_clear_eventually,
            &found_existing](const db::file &file, const db::path &path,
                             const db::item &item) {
        namespace fs = std::filesystem;
        auto full_path = fs::path(directory_path) /
                         db::strip_pb_mount_path(path.path) / file.name;
        std::cout << "Checking " << full_path << std::endl;
        if (!fs::exists(full_path)) {
          files_to_clear.emplace(file.oid);
          items_to_clear_probably.emplace(item.oid);
	  items_to_clear_probably.emplace(file.book_id);
        } else {
          // TODO check hash -- I have no idea what hash function is it.
          if (false /* TODO CHECK HASH */) {
            files_to_clear.emplace(file.oid);
            items_to_clear_probably.emplace(item.oid);
          } else {
            items_not_to_clear_eventually.emplace(item.oid);
            found_existing = true;
          }
        }
      });

  for (int id : items_not_to_clear_eventually) {
    items_to_clear_probably.erase(id);
  }

  if (dry_run) {
    std::cout << "Files to delete: ";
    std::ostream_iterator<int> it(std::cout, ", ");
    std::copy(files_to_clear.begin(), files_to_clear.end(), it);
    std::cout << std::endl;
    std::cout << "Items to delete: ";
    std::copy(items_to_clear_probably.begin(), items_to_clear_probably.end(),
              it);
    std::cout << std::endl;
  } else if (!found_existing) {
    std::cout << "Refusing to delete all items from DB" << std::endl;
  } else {
    std::vector files(files_to_clear.begin(), files_to_clear.end());
    std::vector items(items_to_clear_probably.begin(),
                      items_to_clear_probably.end());
    sql << "DELETE from Items WHERE OID in (:ids)", use(items);
    sql << "DELETE from FILES WHERE OID in (:ids)", use(files);
  }
}

int main(int argc, char **argv) {
  std::string db_path;
  boost::optional<std::string> title_part;
  std::string output_format;
  boost::optional<int> book_id;
  boost::optional<std::string> files_directory;
  bool dry_run = false;
  bool clear_orphans = false;
  std::map<std::string, std::unique_ptr<getnotes2::output::formatter>>
      formatters;
  formatters["simple"] = std::unique_ptr<getnotes2::output::formatter>(
      new getnotes2::output::simple_formatter());
  formatters["Pretty"] = std::unique_ptr<getnotes2::output::formatter>(
      new getnotes2::output::simple_formatter());
  formatters["sexp"] = std::unique_ptr<getnotes2::output::formatter>(
      new getnotes2::output::sexp_formatter());

  po::options_description desc("All options");
  desc.add_options()("help", "produce help message")(
      "dry-run",
      po::value<bool>(&dry_run)->default_value(false)->implicit_value(true))(
      "database-path,d", po::value<std::string>(&db_path)->default_value(
                             "/media/PB631/system/config/books.db"))(
      "output-format,o",
      po::value<std::string>(&output_format)->default_value("simple"))(
      "book-title,t", po::value<boost::optional<std::string>>(&title_part)
                          ->implicit_value(boost::optional<std::string>("")))(
      "book-id,b", po::value<boost::optional<int>>(&book_id))(
      "clear-books-not-present,c",
      po::value<boost::optional<std::string>>(&files_directory))
    ("clear-orphaned-items,i",po::value<bool>(&clear_orphans)->default_value(false)->implicit_value(true));
  po::variables_map vm;

  po::store(po::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return 1;
  }

  if (formatters.find(output_format) != formatters.end()) {
    getnotes2::output::formatter &fmt = *formatters[output_format];
    session sql(*factory_sqlite3(), db_path);
    sql.once << "PRAGMA foreign_keys = ON;";

    if (title_part) {
      find_books(sql, *title_part, fmt);
    }

    if (book_id) {
      find_notes(sql, *book_id, fmt);
    }

    if (files_directory) {
      clear_books(sql, *files_directory, dry_run);
    }
    if (clear_orphans) {
      clear_orphaned_items(sql, dry_run);
    }

    return 0;
  } else {
    std::cout << "Unrecognized output format " << output_format << std::endl;
    return 1;
  }
}
