#ifndef POCKETBOOK_TYPES_H
#define POCKETBOOK_TYPES_H

#include <algorithm>
#include <chrono>
#include <map>
#include <optional>
#include <string>
#include <variant>

#include "pocketbook_tables.hpp"
#include <nlohmann/json.hpp>

namespace getnotes2::hl_types {
enum types { note, highlight, bookmark };

struct location {
  std::string anchor;
  int created_timestamp;
};

struct note_text {
  std::string text;
};

struct highlight_text {
  std::string begin;
  std::string end;
  std::string text;
};

struct parsed_tag {
  int tag_id;
  std::variant<location, note_text, highlight_text> val;
};

// TODO add final_tag builder instead of initially populating it with silly (0)
// values
struct final_tag {
  int tag_id;
  int page;
  std::chrono::time_point<std::chrono::system_clock> timestamp;
  std::optional<std::string> highlighted_text;
  std::optional<std::string> note_text;
};

std::optional<parsed_tag> parse_tag(const db::tag &input);
int parse_page_in_anchor(const std::string &anchor);

template <class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template <class... Ts> overloaded(Ts...)->overloaded<Ts...>;

template <class It> std::map<int, final_tag> parse_tag_list(It first, It last) {
  std::map<int, final_tag> ret;
  std::for_each(first, last, [&ret](const db::tag &el) {
    auto parsed = parse_tag(el);
    auto tt = ret.try_emplace(el.item_id, final_tag());
    std::visit(overloaded{
        [&tt](const location &l) {
          tt.first->second.timestamp =
              std::chrono::system_clock::from_time_t(l.created_timestamp);
          tt.first->second.page = parse_page_in_anchor(l.anchor);
        },
        [&tt](const note_text &t) { tt.first->second.note_text = t.text; },
        [&tt](const highlight_text &h) {
          tt.first->second.highlighted_text = h.text;
        }}, parsed->val);
  });
  return ret;
}

using nlohmann::json;

void to_json(json &j, const location &l);
void from_json(const json &j, location &l);
void to_json(json &j, const note_text &l);
void from_json(const json &j, note_text &l);
void to_json(json &j, const highlight_text &l);
void from_json(const json &j, highlight_text &l);
void to_json(json &j, const types &l);
void from_json(const json &j, types &l);
} // namespace getnotes2::hl_types

#endif /* POCKETBOOK_TYPES_H */
