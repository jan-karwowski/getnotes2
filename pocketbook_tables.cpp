#include "pocketbook_tables.hpp"

using namespace soci;

namespace getnotes2::db {

soci::rowset<book> get_books_by_title_part(soci::session &sql,
                                           const std::string &title_part) {
  return sql.prepare << "select OID, Title, Authors, TimeAdd from Books WHERE "
                        "title LIKE :tp",
         use("%" + title_part + "%", "tp");
}

soci::rowset<tag> get_tags_by_book_id(soci::session &sql, int id) {
  return sql.prepare << "select t.OID, t.ItemID, t.TagID, t.TimeEdt, t.Val "
                        "FROM tags as t inner join items as i on t.ItemID = "
                        "i.OID where i.parentId = :bookid AND t.Val LIKE '{%'",
         use(id, "bookid");
}

std::string strip_pb_mount_path(const std::string &path) {
  // remove /mnt/ext1, probably can be better than that
  return path.substr(10);
}

soci::rowset<item> get_all_items(soci::session &sql) {
  return sql.prepare << "select * FROM items;";
}

void delete_items_by_oid(soci::session& sql, const std::vector<int>& oids) {
  (sql << "DELETE FROM Items where OID in (:val)", use(oids));
}


//TODO support signalling nonexistent ID
item get_top_parent(soci::session &sql, int item_oid) {
  item ret;
  std::optional<int> the_id = item_oid;
  soci::statement st =
      (sql.prepare << "select OID, ParentID, TypeID, State, TimeAlt, HashUUID "
                      "FROM Items where OID=:val",
       use(item_oid), into(ret));

  do {
    item_oid = *the_id;
    assert(st.execute(true));
    assert(item_oid == ret.oid);
    the_id = ret.parent;    
  } while (the_id && *the_id != 0);
  return ret;
}

} // namespace getnotes2::db
