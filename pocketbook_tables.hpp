#ifndef POCKETBOOK_TABLES_H
#define POCKETBOOK_TABLES_H

#include <iostream>

#include <optional>
#include <string>

#include <soci.h>
#include <boost-optional.h>

namespace getnotes2::db {
struct book {
  int oid;
  std::string title;
  std::optional<std::string> authors;
  int time_added;
};

struct file {
  int oid;
  int book_id;
  int path_id;
  std::string name;
  int len;
  int ord;
};

struct path {
  int oid;
  int stor_id;
  std::string path;
};

struct item {
  int oid;
  int type_id;
  std::optional<int> parent;
  int state;
  int time_alt;
  std::string hash_uuid;
};

struct tag {
  int oid;
  int item_id;
  int tag_id;
  int time_edited;
  std::string val;
};

soci::rowset<book> get_books_by_title_part(soci::session &sql,
                                           const std::string &title_part);
soci::rowset<tag> get_tags_by_book_id(soci::session &sql, int id);

soci::rowset<item> get_all_items(soci::session &sql);
void delete_items_by_oid(soci::session& sql, const std::vector<int>& oids);

item get_top_parent(soci::session &sql, int item_oid);

template <class Action>
void for_each_file_in_db(soci::session &sql, Action action) {
  soci::rowset<soci::row> rs =
      (sql.prepare
       << "select p.OID as poid, p.StorID, p.Path, f.OID as foid, f.BookID, "
          "f.PathID, f.Name, f.Len, f.Ord from Paths as p inner "
          "join files as f on p.OID=f.PathID");
  for (const auto &r : rs) {
    file f;
    path p;

    f.book_id = r.get<int>("BookID");
    f.len = r.get<int>("Len");
    f.name = r.get<std::string>("Name");
    f.oid = r.get<int>("foid");
    f.ord = r.get<int>("Ord");
    f.path_id = r.get<int>("PathID");
    p.oid = r.get<int>("poid");
    p.path = r.get<std::string>("Path");
    p.stor_id = r.get<int>("StorID");

    item i = db::get_top_parent(sql, f.book_id);
    
    action(f, p, i);
  }
}

std::string strip_pb_mount_path(const std::string &path);

} // namespace getnotes2::db

namespace soci {
template <> struct type_conversion<getnotes2::db::book> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */,
                        getnotes2::db::book &b) {
    b.oid = v.get<int>("OID");
    b.title = v.get<std::string>("Title");
    b.time_added = v.get<int>("TimeAdd");

    if (v.get_indicator("Authors") == i_null) {
      b.authors = std::optional<std::string>();
    } else {
      b.authors = std::make_optional(v.get<std::string>("Authors"));
    }
  }

  static void to_base(const getnotes2::db::book &b, values &v, indicator &ind) {
    v.set("OID", b.oid);
    v.set("Title", b.title);
    v.set("TimeAdd", b.time_added);
    v.set("Authors", b.authors.value_or(""),
          b.authors.has_value() ? i_ok : i_null);
    ind = i_ok;
  }
};

template <> struct type_conversion<getnotes2::db::file> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */,
                        getnotes2::db::file &f) {
    f.oid = v.get<int>("OID");
    f.book_id = v.get<int>("bookId");
    f.path_id = v.get<int>("pathId");
    f.name = v.get<std::string>("name");
    f.len = v.get<int>("len");
    f.ord = v.get<int>("ord");
  }

  static void to_base(const getnotes2::db::file &f, values &v, indicator &ind) {
    v.set("OID", f.oid);
    v.set("bookId", f.book_id);
    v.set("pathId", f.path_id);
    v.set("name", f.name);
    v.set("len", f.len);
    v.set("ord", f.ord);
    ind = i_ok;
  }
};

template <> struct type_conversion<getnotes2::db::item> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */,
                        getnotes2::db::item &i) {
    i.oid = v.get<int>("OID");
    i.type_id = v.get<int>("TypeID");
    if (v.get_indicator("ParentID") == i_null) {
      i.parent = std::optional<int>();
    } else {
      i.parent = std::make_optional(v.get<int>("ParentID"));
    }
    i.state = v.get<int>("State");
    i.time_alt = v.get<int>("TimeAlt");
    i.hash_uuid = v.get<std::string>("HashUUID");
  }

  static void to_base(const getnotes2::db::item &i, values &v, indicator &ind) {
    v.set("OID", i.oid);
    v.set("TypeID", i.type_id);
    v.set("ParentID", i.parent.value_or(-1),
          i.parent.has_value() ? i_ok : i_null);
    v.set("State", i.state);
    v.set("TimeAlt", i.time_alt);
    v.set("HashUUID", i.hash_uuid);

    ind = i_ok;
  }
};

template <> struct type_conversion<getnotes2::db::tag> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */,
                        getnotes2::db::tag &t) {
    t.oid = v.get<int>("OID");
    t.item_id = v.get<int>("ItemID");
    t.tag_id = v.get<int>("TagID");
    t.time_edited = v.get<int>("TimeEdt");
    t.val = v.get<std::string>("Val");
  }

  static void to_base(const getnotes2::db::tag &t, values &v, indicator &ind) {
    v.set("OID", t.oid);
    v.set("ItemID", t.item_id);
    v.set("TagID", t.tag_id);
    v.set("TimeEdt", t.time_edited);
    v.set("Val", t.val);

    ind = i_ok;
  }
};

template <> struct type_conversion<getnotes2::db::path> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */,
                        getnotes2::db::path &p) {
    p.oid = v.get<int>("OID");
    p.path = v.get<std::string>("Path");
    p.stor_id = v.get<int>("StorID");
  }

  static void to_base(const getnotes2::db::path &p, values &v, indicator &ind) {
    v.set("OID", p.oid);
    v.set("Path", p.path);
    v.set("StorID", p.stor_id);

    ind = i_ok;
  }
};

} // namespace soci

#endif /* POCKETBOOK_TABLES_H */
