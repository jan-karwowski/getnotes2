#ifndef FORMATTERS_H
#define FORMATTERS_H

#include <map>

#include "pocketbook_tables.hpp"
#include "pocketbook_types.hpp"
#include <soci.h>

namespace getnotes2::output {
class formatter {
public:
  virtual void format_book_list(soci::rowset<getnotes2::db::book> &) const = 0;
  virtual void
  format_notes(const std::map<int, hl_types::final_tag> &notes) const = 0;
};

class simple_formatter : public formatter {
  void format_book_list(soci::rowset<getnotes2::db::book> &) const;
  virtual void
  format_notes(const std::map<int, hl_types::final_tag> &notes) const;
};

class sexp_formatter : public formatter {
  void format_book_list(soci::rowset<getnotes2::db::book> &) const;
  virtual void
  format_notes(const std::map<int, hl_types::final_tag> &notes) const;
};
} // namespace getnotes2::output

#endif /* FORMATTERS_H */
