#include "formatters.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

using std::cout;
using std::endl;
using std::string;

namespace getnotes2::output {
void simple_formatter::format_book_list(
    soci::rowset<getnotes2::db::book> &list) const {
  for (auto &book : list) {
    string aut = book.authors ? *book.authors + ". " : "";
    // TODO time formatting
    cout << book.oid << ": " << aut << book.title << " " << book.time_added
         << endl;
  }
}

void simple_formatter::format_notes(
    const std::map<int, hl_types::final_tag> &notes) const {
  std::vector<hl_types::final_tag> vec(notes.size());
  std::transform(notes.begin(), notes.end(), std::back_inserter(vec),
                 [](const auto &p) { return p.second; });
  std::sort(vec.begin(), vec.end(),
            [](const auto &x, const auto &y) { return x.page < y.page; });
  for(const auto &t : vec) {
    std::cout << " - Page " << t.page << " \"" << t.highlighted_text.value_or("") << "\", " << t.note_text.value_or("") << std::endl;
  }
}

void sexp_formatter::format_book_list(
    soci::rowset<getnotes2::db::book> &list) const {
  cout << '(';
  for (auto &book : list) {
    string aut = book.authors ? "author \"" + *book.authors + "\" " : "";
    cout << "(oid " << book.oid << " " << aut << "title \"" << book.title
         << "\" timeAdded " << book.time_added << ") ";
  }
  cout << ')' << endl;
}

void sexp_formatter::format_notes(
    const std::map<int, hl_types::final_tag> &notes) const {
  std::cout << "(";
  for(const auto& p: notes) {
    auto highlight_pentry = p.second.highlighted_text ?
      "highlight "+*p.second.highlighted_text+" " :
      "";
    auto text_pentry = p.second.note_text ?
      "text "+*p.second.note_text+" " :
      "";
    std::cout << "(id " << p.first << " page " << p.second.page << " date " <<
      std::chrono::system_clock::to_time_t(p.second.timestamp) << " " <<
      highlight_pentry << text_pentry << ") ";
  }
  std::cout << ")" << std::endl;
}
} // namespace getnotes2::output
