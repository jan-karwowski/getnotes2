;;; pb.el -- browse pocketbook notes through ivy

;; Author: Jan Karwowski
;; URL:
;; Version: 0.1
;; Keywords: pocketbook notes ivy
;; Package-Requires (ivy)

;;; Commentary:
;;; TODO

;;; Code:


;;; pb.el ends here

(require 'ivy)

(defgroup pb-el nil
  "Customization group for pb-el."
  :group 'tools)

(defcustom pb-el-getnotes-path "getnotes2"
  "A path to getnotes executable."
  :type 'string
  :group 'pb-el
  :package-version '(pb-el . "0.0"))

(defcustom pb-el-mount-path "/media/PB631"
  "A path where pocketbook is mounted."
  :type 'string
  :group 'pb-el
  :package-version '(pb-el . "0.0"))


(defun pb-el--db-path ()
  "Returuns path do db."
  (string-join (list pb-el-mount-path "system" "config" "books.db") "/"))

(defun pb-el--command-start ()
  "Retuns initial comand."
  (string-join  (list pb-el-getnotes-path "-d" (pb-el--db-path)) " "))

(defun pb-el--book-list ()
  "Call getnotes and retruns list of books on PB."
  (with-temp-buffer
    (shell-command (string-join  (list (pb-el--command-start) "-o" "sexp" "-t") " ") (current-buffer) nil)
    (goto-char (point-min))
    (mapcar (lambda (book)
	      (list (string-join (list (number-to-string (plist-get book 'oid)) (plist-get book 'author) (format-time-string "%Y-%m-%d %H:%M" (plist-get book 'timeAdded)) (plist-get book 'title)) ":")  (plist-get book 'oid)))
	    (read (current-buffer)))))

(defun pb-el-get-notes ()
  "Show ivy completion with selection of notes."
  (interactive)
  (ivy-read "Book: " (pb-el--book-list)
	    :history 'ivy-pb-history
	    :action '(1 ("b" (lambda (x) (let (
					       (oid (car (cdr x)))
					       (buff (get-buffer-create "*PB Notes*")))
					   (shell-command (string-join (list (pb-el--command-start) "-o" "Pretty" "notes" "-b" (number-to-string oid)) " ") buff)
					   (switch-to-buffer-other-window buff)))))))

(provide 'pb)
;; pb.el ends here
